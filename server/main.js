import '../imports/api/questionFeedback.js';
import '../imports/api/questions.js';

import { Meteor } from 'meteor/meteor';
import { Questions } from '../imports/api/questions.js';


// import { FurtherMathsQuestions } from '../imports/api/FurtherMathsDatabase.js';
// import { QuestionFeedback } from '../imports/api/questionFeedback.js';


Meteor.startup(() => {
  // TODO need to look into the uploading of images which was covered in one of the courses taken
  console.log("Database contains " + Questions.find({}).count());

  if(Questions.find().count() == 0) {
    console.log("No entries in question database, starting us off");
    Meteor.call('questions.insert');
  }

  // const totalNumQuestions = FurtherMathsQuestions['questions'].length;
  // console.log(totalNumQuestions);
  // Maybe just add this logic when updating
  // Make an entry for each question so only need to update
  // for (var i = 1; i <= totalNumQuestions; i++) {
  //   QuestionFeedback.insert({
  //     id: i,
  //     too_easy: 0,
  //     just_right: 0,
  //     too_hard: 0
  //   });
  // }
});
