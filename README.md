# README #
Project to create a web app for VCE students to use to be able to quiz themselves. The app is built off the Meteor framework and the running edition can be found at [here](practicevce.online)

# Owners #
* Dylan Maccora [(Bitbucket)](htps://bitbucket.org/maccod)
* Damien Vermeer