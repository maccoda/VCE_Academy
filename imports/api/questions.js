import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import { Meteor } from 'meteor/meteor';

export const Questions = new Mongo.Collection('questions');

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish('questions', function tasksPublication() {
    return Questions.find();
  });
}

Meteor.methods({
    'questions.insert' () {
        Questions.insert({
            qnId: 0,
            subject: "Further Maths",
            topic: "Financial",
            questionText: "Some Question",
            corrAns: "a",
            ans: ["b","c","d","e"],
            solutionText: "An answer is always correct"
        });
    },
    'questions.upsert' (qnId, data) {
        console.log(qnId);
        check(qnId, Number);
        console.log("Upserting question with index " + qnId);

        const id  = Questions.upsert({
            qnId: qnId
        }, {
            $setOnInsert: {
                qnId: qnId
            },
            $set: {
                subject: data['subject'],
                topic: data['topic'],
                questionText: data['questionText'],
                questionImg: data['questionImg'],
                corrAns: data['corrAns'],
                ans: data['ans'],
                solutionText: data['solutionText'],
                solutionImg: data['solutionImg'],
                metaData: data['metaData']
            }
        });
        console.log("Upsert complete");

        console.log("Database now contains " + Questions.find({}).count());
    },
    'questions.count'() {
        return Questions.find({}).count();
    }
})
