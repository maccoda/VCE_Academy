/*
 * This file is similar to the subjectsList file but will be a way to keep track
 * of topics in use which will make it easier to produce the drop down menu as well
 * as add in new topics when they are required.
 */
 // NOTE Might keep this as a backup for now and maybe make new topics by simple injection.
export const TopicsList = [
  {
    subject: 'Further Maths',
    topics: [
      {name:"Matricies"},
      {name:"Geometry & Measurement"},
      {name:"Graphs & Relations"},
      {name:"Network & Decision Mathematics"},
      {name:"Data Analysis"}
    ]
  },
  {
    subject:"Maths Methods",
    topics: [
      {name:"Functions & Graphs"},
      {name:"Algebra"},
      {name:"Calculus"}
    ]
  }
];
