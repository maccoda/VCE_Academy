import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import { Meteor } from 'meteor/meteor';

export const QuestionFeedback = new Mongo.Collection('question_feedback');

Meteor.methods({
  'feedback.insert'(qnId){
    check(qnId, Number);
    // Always inserts an empty record
    QuestionFeedback.insert({
      qnId: qnId,
      too_easy: 0,
      just_right: 0,
      too_hard: 0
    });
  },
  'feedback.incTooEasy'(qnId){
    check(qnId, Number);
    QuestionFeedback.upsert(
      {qnId:qnId}, {
        $setOnInsert: {
          qnId: qnId,
          just_right: 0,
          too_hard: 0
        },
        $inc: {
          too_easy: 1
        }
      }
    );
  },
  'feedback.incJustRight'(qnId){
    check(qnId, Number);
    QuestionFeedback.upsert(
      {qnId:qnId}, {
        $setOnInsert: {
          qnId: qnId,
          too_easy: 0,
          too_hard: 0
        },
        $inc: {
          just_right: 1
        }
      }
    );
  },
  'feedback.incTooHard'(qnId){
    check(qnId, Number);
    QuestionFeedback.upsert(
      {qnId:qnId}, {
        $setOnInsert: {
          qnId: qnId,
          too_easy: 0,
          just_right: 0
        },
        $inc: {
          too_hard: 1
        }
      }
    );
  },
  'feedback.writeComment': function(qnId, comment, dbRev){
    const fs = require('fs');
    check(qnId, Number);
    const fileName = "feedback.csv";
    // Get content for log
    const currentDate = new Date();
    const date = currentDate.getFullYear() + "-" + (currentDate.getMonth() + 1) + "-" + currentDate.getDate();
    const time = currentDate.getHours() + ":" + currentDate.getMinutes() + ":" + currentDate.getSeconds();
    // Format the data to the following in CSV
    // Date | Time | Database Revision | Question ID | Comment
    const bufferToWrite = date + "," + time + "," + dbRev + "," + qnId + "," + comment + "\n";
    console.log(bufferToWrite);

    fs.appendFile(fileName, bufferToWrite);
  }
});
