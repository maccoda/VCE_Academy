/*
 * This file is simply here for initial stages. Later it can be automatically created
 * once more subjects have questions. For now it is a place holder just for show.
 *
 */
export const SubjectsList =
[
  {name:'Further Maths', available:true},
  {name:'Physics',       available:false},
  {name:'Maths Methods', available:true}
];
