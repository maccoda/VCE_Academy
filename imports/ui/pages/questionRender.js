import './questionRender.html';

import { Template } from 'meteor/templating';
import { Session } from 'meteor/session';

import { parseApiRequest } from './quiz.js';


export let answers;
export let corrAns;

export const randomizeOrder = function(allQuestions) {
   // Populate array randomly
   const questionIndex = Session.get("qnIndex");

   corrAns = parseApiRequest(allQuestions[questionIndex]['corrAns']);
   answers = new Array(0);
   var tempArray = [{text:allQuestions[questionIndex]['corrAns']},
                    {text:allQuestions[questionIndex]['ans'][0]},
                    {text:allQuestions[questionIndex]['ans'][1]},
                    {text:allQuestions[questionIndex]['ans'][2]},
                    {text:allQuestions[questionIndex]['ans'][3]}];
   for (var i = 0; i < 5; i++) {
     const index = Math.floor(Math.random() * tempArray.length);
     answers.push(tempArray[index]);
     // Remove from temp array
     tempArray.splice(index,1);
   }
}

export const ansToObject = function(ansArray, correctAnswer) {
  answers = [
    {text: correctAnswer},
    {text: ansArray[0]},
    {text: ansArray[1]},
    {text: ansArray[2]},
    {text: ansArray[3]}
  ];
}

Template.question.helpers({
  getAnswers: function() {
    for (var i = 0; i < answers.length; i++) {
      answers[i]['text'] = parseApiRequest(answers[i]['text']);
    }
    return answers;
  },
  renderImage: function() {
    if (this.questionImg != "") {
      // There is an image to render, do some work
      const subject = FlowRouter.getParam("subject");
      let directory;
      if (subject == "Further Maths") {
        directory = "Further/";
      } else if (subject == "Maths Methods"){
        directory = "Methods/"
      } else {
        console.log(Session.get("subject"));
        switch (Session.get("subject")) {
          case "Further Maths":
            directory = "Further/";
            break;
          case "Maths Methods":
            directory = "Methods/"
            break;
          default:
            console.error("Incorrect subject");
            break;
        }
      }
      return '<img src="/QuestionImages/' + directory + this.questionImg + '.png" class="image-row img-responsive" alt="' + this.questionImg + '">';
    } else {
      return null;
    }
  },
  parseQuestionText: function() {
    //IDEA This function and the one used for solution is almost identical,
    // should consider making these the same function.
    let text = this['questionText']
    do {
      text = parseApiRequest(text);
    } while (text.indexOf('@') > -1);
    return text;
  }
});
