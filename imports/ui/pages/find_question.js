/*
 * This page is almost an exact replica of quiz.
 * Purpose of this page is to ensure correct rendering and funtonality
*/
import './find_question.html';

import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { $ } from 'meteor/jquery';
import { FlowRouter } from 'meteor/kadira:flow-router';

import { FurtherMathsQuestions } from '../../api/FurtherMathsDatabase.js';
import { QuestionFeedback } from '../../api/questionFeedback.js';
import { SubjectsList } from '../../api/subjectsList.js';

let answers;
let corrAns;
let chosenTopicQuestions;
let totalNumQuestions;
let updateQuestion;

const parseApiRequest = function(inputString) {
  const preAppend = '<img src="http://chart.googleapis.com/chart?cht=tx&&chf=bg,s,00000000&chl=';
  const postAppend = '" border=0>';

  var result = inputString.replace('@',preAppend);
  result = result.replace('@',postAppend);
  return result;
};

const randomizeOrder = function(allQuestions) {
   // Populate array randomly
   const questionIndex = Template.instance().currQuestionIndex.get();

   corrAns = parseApiRequest(allQuestions[questionIndex]['answer']);
   answers = new Array(0);
   var tempArray = [{text:allQuestions[questionIndex]['answer']},
                    {text:allQuestions[questionIndex]['otherOptions'][0]},
                    {text:allQuestions[questionIndex]['otherOptions'][1]},
                    {text:allQuestions[questionIndex]['otherOptions'][2]},
                    {text:allQuestions[questionIndex]['otherOptions'][3]}];
   for (var i = 0; i < 5; i++) {
     const index = Math.floor(Math.random() * tempArray.length);
     answers.push(tempArray[index]);
     // Remove from temp array
     tempArray.splice(index,1);
   }
}

Template.find_question.onCreated(function() {
  document.title = "Find Question - Practice VCE";
  chosenTopicQuestions = FurtherMathsQuestions['questions']; // Get all the questions
  totalNumQuestions = chosenTopicQuestions.length;
  this.currQuestionIndex = new ReactiveVar(0); // Don't need randomization here
  randomizeOrder(chosenTopicQuestions);
});

Template.find_question.helpers({
  currQuestion:function() {
    const questionIndex = Template.instance().currQuestionIndex.get();
    const currentQuestion = chosenTopicQuestions[questionIndex];
    var question = currentQuestion;
    do {
      question['questionText'] = parseApiRequest(currentQuestion['questionText']);
    } while (question['questionText'].indexOf('@') > -1);
    question['dbRev'] = FurtherMathsQuestions['properties']['revnumber'];
    question['solutionText'] = currentQuestion['solutionText'];

    // Parse solution but this may have multiple
    // HACK This is here so I don't kill everything
    let iter = 0;
    do {
      question['solutionText'] = parseApiRequest(question['solutionText']);
      iter++;
    } while (question['solutionText'].includes('@') && iter < 50);
    console.log(question);
    updateQuestion = JSON.parse(JSON.stringify(question));
    console.log(updateQuestion);
    return question;
  },
  getAnswers: function() {
    for (var i = 0; i < answers.length; i++) {
      answers[i]['text'] = parseApiRequest(answers[i]['text']);
    }
    return answers;
  },
  renderImage: function() {
    if (this.questionImage != "") {
      // There is an image to render, do some work
      return '<img src="/QuestionImages/' + this.questionImage + '.png" class="image-row">';
    } else {
      return null;
    }
  },
  renderSolImage: function() {
    if (this.solutionImg != "") {
      // There is an image to render, do some work
      return '<img src="/QuestionImages/' + this.solutionImg + '.png" class="image-row">';
    } else {
      return null;
    }
  },
  currFeedback: function() {
    // return QuestionFeedback.find({id:Template.instance().currQuestionIndex.get()});
    var obj = {};
    obj['qnId'] = Template.instance().currQuestionIndex.get() + 1;
    return QuestionFeedback.findOne(obj);
  },
  rawQuestion: function() {
    const questionIndex = Template.instance().currQuestionIndex.get();
    return currentQuestion = FurtherMathsQuestions['questions'][questionIndex];
  },
  rawAnswers: function() {
    const questionIndex = Template.instance().currQuestionIndex.get();
    const currentQuestion = FurtherMathsQuestions['questions'][questionIndex];
    var tempArray = [{text:currentQuestion['otherOptions'][0]},
                     {text:currentQuestion['otherOptions'][1]},
                     {text:currentQuestion['otherOptions'][2]},
                     {text:currentQuestion['otherOptions'][3]}];
    return tempArray;
  },
  availableSubject: function() {
    return SubjectsList.filter(function(elem) {
      return elem['available'];
    });
  }
});

Template.find_question.events({
  "click #submit_ans": function(event, instance){
     // Find which was selected
     var choice = $("input[name=optradio]:checked").val();
     const selectedID = $("input[name=optradio]:checked").attr("id");

     // Were they correct?
     if (choice == undefined) {
       // IDEA Maybe make this a modal as well
       alert("You didn't choose an answer");
     } else{
        const divId = "div-" + selectedID;

        if (choice == corrAns) {
          $("#"+divId).addClass("cor-ans");
          //$("#ans-result").modal('show'); // Can't be bothered showing modal
          // Tidy up that mess
          $(".ans-div").removeClass('incor-ans');
       } else {
         $("#"+divId).addClass("incor-ans");
       }
     }
  },
  "click #find_qn_btn": function(event, instance) {
    const nextId = $('#qn_id').val();
    // Perform range check
    if(nextId > 0 && nextId < totalNumQuestions) {
      // Zero indexed internally id one-indexed
      instance.currQuestionIndex.set(nextId - 1);
    } else {
      alert("Index out of bounds");
    }

  },
  "click #renderBtn": function(event, instance) {
    console.log("Render has been pressed");
    // Assumption is want to just re-render not change anything in the database
    // NOTE will be a lot easier if just directly update the database...
    // I think will just do that and take the risks
    updateQuestion['questionText'] = $('#qnInput').val();

    console.log("Setting qn id to " + (question['id']-1));


  }
});
