  import './topics.html';

import { Template } from 'meteor/templating';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { $ } from 'meteor/jquery';

import { FurtherMathsQuestions } from '../../api/FurtherMathsDatabase.js';
import { MathsMethodsQuestions } from '../../api/FurtherMathsDatabase.js';
const uniqueTopics = [];

Template.topic_well.onCreated(function(){
  document.title = 'Topics - Practice VCE'
  // Find out which subject we are using
  const subject = FlowRouter.getParam("subject");
  // Will populate the topics array
  let questions;
  if (subject == "Further Maths") {
    questions = FurtherMathsQuestions['questions'];
  } else {
    questions = MathsMethodsQuestions['questions'];
  }
  // Time to functional program this shit up
  uniqueTopics.length = 0;
  const topics = questions.map(function(elem) {
    return elem["topic"];
  });
  $.each(topics, function(index, element){
    if($.inArray(element, uniqueTopics) == -1) uniqueTopics.push(element);
  })
});
Template.topic_well.helpers({
  subject: function() {
    return FlowRouter.getParam("subject");
  }
});

Template.topics.helpers({
  topicList: function() {
    const topics = [];
    uniqueTopics.forEach(function(element, index, array){
      topics.push({name:element});
    });
    console.log(topics);
    return topics;
  },
});

Template.topics.events({
  "click #start-quiz": function(event, instance){
    // Find which topics were chosen they are referenced by their index
    const chosenTopics = $('input[type=checkbox]:checked');
    const params = {selectedTopics:[], subject:FlowRouter.getParam("subject")};
    const queryParams= {};


    chosenTopics.each(function(index) {
      params['selectedTopics'].push(uniqueTopics[this.id]);
    });
    console.log(params);
    if (params['selectedTopics'].length > 0) {
      FlowRouter.go('/:subject/quiz:selectedTopics', params, queryParams);
    } else {
      alert("You forgot to select topics!");
    }
  },
  "click #select_all": function(event, instance) {
    $('.js-btn').addClass('active');
    $('.topic-btn').prop('checked',true);
  }
});
