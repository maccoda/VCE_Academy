import './quiz.html';
import './questionRender.js';
import './solutionRender.js';

import { Template } from 'meteor/templating';
import { $ } from 'meteor/jquery';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session';


import { FurtherMathsQuestions } from '../../api/FurtherMathsDatabase.js';
import { MathsMethodsQuestions } from '../../api/FurtherMathsDatabase.js';
import { QuestionFeedback } from '../../api/questionFeedback.js';
import { Questions } from '../../api/questions.js';
import { answers } from './questionRender.js';
import { randomizeOrder } from './questionRender.js';
import { corrAns } from './questionRender.js';

let chosenTopicQuestions;
let totalNumQuestions;
let prevQuestionQueue = [];
let MaxLengthPrevQuestionQueue;
let selectedDatabase;

const queueFull = function() {
  if(prevQuestionQueue.length > MaxLengthPrevQuestionQueue) {
    return true;
  } else {
    return false;
  }
}


const nextQuestion = function(currIndex) {
  if(currIndex != null) {
    // Isn't the first time, add to linked list
    prevQuestionQueue.push(currIndex);
    if(queueFull()) {
      prevQuestionQueue.shift();
    }
  }
  // Select random index for question
  let index;
  do {
    index = Math.floor(Math.random() * chosenTopicQuestions.length);
  } while (prevQuestionQueue.indexOf(index) >= 0);

  return index;
 };

export const parseApiRequest = function(inputString) {
  const preAppend = '<img src="http://chart.googleapis.com/chart?cht=tx&&chf=bg,s,00000000&chl=';
  const postAppend = '" border=0>';

  var result = inputString.replace('@',preAppend);
  result = result.replace('@',postAppend);
  return result;
};

let collectionHandler;

const setSelectedQuestions = function(selectedTopics, subject) {
  const allQuestions = Questions.find({}).fetch();
  chosenTopicQuestions = allQuestions.filter(function(elem) {
    return (elem['subject'] == subject) && (selectedTopics.indexOf(elem['topic']) >= 0);
  });
}

Template.quiz.onCreated(function() {
  collectionHandler =  Meteor.subscribe('questions');
  document.title = 'Quiz - Practice VCE';

});

Template.quiz.helpers({
  currQuestion:function() {
    // Get topics from Router
    const selectedTopics = FlowRouter.getParam("selectedTopics").split(",");
    const subject = FlowRouter.getParam("subject");
    setSelectedQuestions(selectedTopics, subject);
    totalNumQuestions = chosenTopicQuestions.length;
    // Cannot be within the last 20% of the current questions
    MaxLengthPrevQuestionQueue = Math.floor(totalNumQuestions / 5);
    Session.set("qnIndex",nextQuestion(null));
    randomizeOrder(chosenTopicQuestions);

    const questionIndex = Session.get("qnIndex");
    const currentQuestion = chosenTopicQuestions[questionIndex];
    var question = currentQuestion;

    return question;
  },
  loading: function() {
    console.log("Loading...");

    return !collectionHandler.ready();
  }
});

Template.quiz.events({
  "click #submit_ans": function(event, instance){
     // Find which was selected
     var choice = $("input[name=optradio]:checked").val();
     const selectedID = $("input[name=optradio]:checked").attr("id");

     // Were they correct?
     if (choice == undefined) {
       // IDEA Maybe make this a modal as well
       alert("You didn't choose an answer");
     } else{
        const divId = "div-" + selectedID;

        if (choice == corrAns) {
          $("#"+divId).addClass("cor-ans");
          $("#ans-result").modal('show');
          // Tidy up that mess
          $(".ans-div").removeClass('incor-ans');
       } else {
         $("#"+divId).addClass("incor-ans");
       }
     }
  },
  "click #skip_ques": function(event, instance){
    Session.set("qnIndex", nextQuestion(Session.get("qnIndex")));
    randomizeOrder(chosenTopicQuestions);
    cleanup();
  },
  "click .feedback_btn": function(event, instance) {
    $(".feedback_btn").removeClass('active-fb-btn');
    $("#" + event.target.id).addClass('active-fb-btn');
  },
  "click #next_ques": function(event, instance){
    const feedback = $(".active-fb-btn").attr("id");
    const currQnIndex = Session.get("qnIndex");
    const currQuestionFeedback = QuestionFeedback.find({currQnIndex});

    // Find which item was selected
    switch (feedback) {
      case "too_easy":
        Meteor.call('feedback.incTooEasy', currQnIndex);
        break;
      case "just_right":
        Meteor.call('feedback.incJustRight', currQnIndex);
        break;
      case "too_hard":
        Meteor.call('feedback.incTooHard', currQnIndex);
        break;
      default:
        // alert("Please help us out for now with some feedback. Thank you");
        break;
    }
    const comments = $("#other_comments").val();
    if (comments.length > 0) {
      Meteor.call('feedback.writeComment', currQnIndex, comments, selectedDatabase['properties']['revnumber']);
    }

    Session.set("qnIndex", nextQuestion(currQnIndex));
    randomizeOrder(chosenTopicQuestions);
    // Clean up your dirty bastard
    cleanup();
  }
});

const cleanup = function() {
  $(".ans-div").removeClass('cor-ans');
  $("input[name=optradio]:checked").prop("checked",false);
  $(".feedback_btn").removeClass('active-fb-btn');
  $("#other_comments").val("");
}
