import './new_question.html';
import './questionRender.js';

import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { $ } from 'meteor/jquery';

import { Questions } from '../../api/questions.js';
import { answers } from './questionRender.js';
import { ansToObject } from './questionRender.js';
import { SubjectsList } from '../../api/subjectsList.js';
import { TopicsList } from '../../api/topicsList.js'


let collectionHandler;
let currentQuestion;

Template.new_question.onCreated(function() {
  collectionHandler = Meteor.subscribe('questions');
  document.title = "New Question - Practice VCE";
  updateTotalNumber();
  Session.set("qnIndex", 0);
  this.access = new ReactiveVar(false);
  // this.currQuestionIndex = new ReactiveVar(1);
});
let final;
Template.new_question.helpers({
  'currQuestion':function() {

    // const questionIndex = Template.instance().currQuestionIndex.get();

    // currentQuestion = Questions.findOne({qnId:Session.get("qnIndex")});
    const all = Questions.find({}).fetch();
    final = all.filter(function(elem) {
      return elem['qnId'] == Session.get("qnIndex");
    })[0];
    ansToObject(final['ans'], final['corrAns']);
    Session.set("subject", final['subject']);

    // HACK This is a work around for the time being until I can work out why
    // Questions.findOne() returns undefined the second time
    return final;
    // return Questions.find({qnId:questionIndex}).fetch()[0];
  },
  'getAnswers': function() {
    const ansArray = final['ans'];
      if (ansArray == undefined) {
      return [{text:""},{text:""},{text:""},{text:""}]
    } else {
      const result = ansArray.map(function(elem) {
        return {text: elem};
      });
      return result;
    }
  },
  'loading': function() {
    console.log("Loading...");

    return !collectionHandler.ready();
  },
  availableSubject: function() {
    return SubjectsList.filter(function(elem) {
      return elem['available'];
    });
  },
  access: function() {
    return Template.instance().access.get();
  },
  currSubject: function() {
    return (this['name'] == final['subject'])
  },
  currTopic: function() {
    return (this['name'] == final['topic'])
  },
  topicOfSubject: function() {
    const currentSubject = final['subject'];
    console.log("Checking topics");
    const topics = TopicsList.filter(function(elem) {
      return elem['subject'] == currentSubject;
    })[0]['topics'];
    return topics;
  }
});

Template.new_question.events({
  "click #submitQn": function(event, instance){
     event.preventDefault();

   const data = populateQuestionData();

   // Add this to the database
   var qnIndex;
   if (Session.get("newQn")) {
     qnIndex = Questions.find({}).fetch().length;
     console.log("New question index " + qnIndex);
     Session.set("newQn", false);
   } else {
     qnIndex = Session.get("qnIndex");
   }
   Meteor.call('questions.upsert',qnIndex, data);
   updateTotalNumber();
   Session.set("qnIndex", qnIndex);


 },
 "click #find_qn_btn": function(event, instance) {
   const nextId = parseInt($('#qnId').val());

   // Perform range check
   if(nextId < Session.get("total")) {
     // Zero indexed internally id one-indexed
    //  instance.currQuestionIndex.set(nextId);
     Session.set("qnIndex", nextId);

   } else {
     alert("Index out of bounds");
   }
 },
 "click #new_qn_btn": function(event, instance) {
   const totalQuestions = Questions.find({}).fetch().length;
   console.log("Currently have " + totalQuestions + " questions");
   Session.set("newQn", true);

  $('#ques').val("");
  $('#quesImage').val("");
  $('#corrAns').val("");
  $('#ans0').val("");
  $('#ans1').val("");
  $('#ans2').val("");
  $('#ans3').val("");
  $('#sol').val("");
  $('#solImage').val("");
  $('#subject').val("");
  $('#topic').val("");
  $('#metaData').val("");

},
"click #accessButton": function(event, instance) {
  const value = $('#accessKey').val();
  if(value == "Damien15Dylan") {
    instance.access.set(true);
  }
  //NOTE If it is wrong you get no notification for the time being
},
});

const populateQuestionData = function() {
  // Get the values
  var data = {};
  data['questionText'] = $('#ques').val();
  data['questionImg'] = $('#quesImage').val();
  data['corrAns'] = $('#corrAns').val();
  data['ans'] = [];
  data['ans'][0] = $('#ans0').val();
  data['ans'][1] = $('#ans1').val();
  data['ans'][2] = $('#ans2').val();
  data['ans'][3] = $('#ans3').val();
  data['solutionText'] = $('#sol').val();
  data['solutionImg'] = $('#solImage').val();
  data['subject'] = $('#subject').val();
  data['topic'] = $('#topic').val();
  data['metaData'] = $('#metaData').val();

  return data;
}

const updateTotalNumber = function() {
  Meteor.call("questions.count", {}, function(error, result){
    if(error){
      console.log("error", error);
    }
    if(result){
      console.log("Total is now " + result);
      Session.set("total", result);
    }
  });
}
