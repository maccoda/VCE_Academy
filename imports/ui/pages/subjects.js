import './subjects.html';

import { SubjectsList } from '../../api/subjectsList.js';

import { Template } from 'meteor/templating';
import { FlowRouter } from 'meteor/kadira:flow-router';

// Template.subject_well.events({
//   "click .js-btn": function(event, instance) {
//     // TODO Need to find which subject was chosen
//     FlowRouter.go('/topics');
//   }
// });
Template.subjects.onCreated(function() {
  document.title = 'Subjects - Practice VCE';
});

Template.subjects.helpers({
  subjectList : function() {
    return SubjectsList;
  },
  pathForSubject: function() {
    const subjectName = this.name;
    const params = {subject:subjectName};
    const queryParams = {};

    return FlowRouter.path("topics", params, queryParams);
  }
});
