import './solutionRender.html';

import { parseApiRequest } from './quiz.js';

Template.solution.helpers({
  renderSolImage: function() {
    if (this.solutionImg != "") {
      // There is an image to render, do some work
      const subject = FlowRouter.getParam("subject");
      let directory;
      if (subject == "Further Maths") {
        directory = "Further/";
      } else {
        directory = "Methods/"
      }
      return '<img src="/QuestionImages/' + directory + this.solutionImg + '.png" class="image-row img-responsive" alt=' + directory + this.solutionImg + '>';
    } else {
      return null;
    }
  },
  parseSolutionText: function() {
    // Parse solution but this may have multiple
    // HACK This is here so I don't kill everything
    let iter = 0;
    let text = this['solutionText'];

    do {
      text = parseApiRequest(text);
      iter++;
    } while (text.indexOf('@') >= 0 && iter < 50);
    return text;
  }
});
