import './navbar.html';

import { SubjectsList } from '../../api/subjectsList.js';

import { Template } from 'meteor/templating';


Template.navbar.helpers({
  subjectList : function() {
    return SubjectsList;
  },
  pathForSubject: function() {
    const subjectName = this.name;
    const params = {subject:subjectName};
    const queryParams = {};

    return FlowRouter.path("topics", params, queryParams);
  }
});
