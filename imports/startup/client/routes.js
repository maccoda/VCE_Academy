import {FlowRouter} from 'meteor/kadira:flow-router';
import {BlazeLayout} from 'meteor/kadira:blaze-layout';

// Import templates so they can load
import '../../ui/layout/vce_academy.js';
import '../../ui/pages/home_page.js';
import '../../ui/pages/topics.js';
import '../../ui/pages/subjects.js';
import '../../ui/pages/quiz.js';
import '../../ui/pages/navbar.js';
import '../../ui/pages/new_question.js';
import '../../ui/pages/find_question.js';
import '../../ui/pages/about.js';

FlowRouter.route('/', {
  title:'Home',
  name:'home',
  action : function(params) {
    BlazeLayout.render('ApplicationLayout', { content: 'home_page' });
  },
});

FlowRouter.route('/subjects', {
  title:'Subjects',
  parent:'home',
  name:'subjects',
  action : function(params) {
    BlazeLayout.render('ApplicationLayout', { content: 'subject_well' });
  },
});

FlowRouter.route('/topics/:subject', {
  title:'Topics',
  name: 'topics',
  parent: 'subjects',
  action : function(params) {
    BlazeLayout.render('ApplicationLayout', { content: 'topic_well' });
  },
});

FlowRouter.route('/:subject/quiz:selectedTopics', {
  title:'Quiz',
  name: 'quiz',
  parent: 'topics',
  action : function(params) {
    name: 'quiz',
    BlazeLayout.render('ApplicationLayout', { content: 'quiz' });
  },
});

FlowRouter.route('/admin_add_ques', {
  title:'Add Question',
  name: 'add',
  parent: 'home',
  action : function(params) {
    BlazeLayout.render('ApplicationLayout', { content: 'new_question' });
  },
});

// FlowRouter.route('/admin_debug', {
//   title:'Find Question',
//   name: 'find_question',
//   action : function(params) {
//     BlazeLayout.render('ApplicationLayout', { content: 'find_question' });
//   },
// });

FlowRouter.route('/about', {
  title:'About',
  name: 'about',
  parent: 'home',
  action : function(params) {
    BlazeLayout.render('ApplicationLayout', { content: 'about' });
  },
});
